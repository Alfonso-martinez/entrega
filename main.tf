//terraform plan -var 'Environment=DEV' && terraform apply -auto-approve -input=false -var 'Environment=DEV'
//terraform destroy -var 'Environment=DEV' -auto-approve
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

variable "Region" {
  type    = string
  default = "us-east-1"
}

provider "aws" {
  region = var.Region
}

variable "ProjectCode" {
  type    = string
  default = "CL00000"
}

variable "ProjectName" {
  type    = string
  default = "CCS-IoT9"
}

variable "Environment" {
  type        = string
  description = " DEV || PRE || PDN"
}

variable "Stage" {
  type    = string
  default = "v1"
}

############## Roles ##############
resource "aws_iam_role" "kinesis_role" {
  name               = "${var.ProjectCode}-${var.ProjectName}-${var.Environment}-kinesis-iot"
  assume_role_policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = {
          Service = ["iot.amazonaws.com", "iotevents.amazonaws.com"]
        }
      },
    ]
  })
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSTransferLoggingAccess"]
  inline_policy {
    name   = "KinesisPolicy"
    policy = jsonencode({
      Version   = "2012-10-17"
      Statement = [
        {
          Action   = ["kinesis:PutRecord", "kinesis:PutRecords"]
          Effect   = "Allow"
          Resource = "${aws_kinesis_stream.stream.arn}"
        },
      ]
    })
  }
  tags = {
    ProjectCode = "${var.ProjectCode}",
    ProjectName = "${var.ProjectName}",
    Environment = "${var.Environment}"
  }
}

resource "aws_iam_role" "lambda_api_kinesis_role" {
  name               = "${var.ProjectCode}-${var.ProjectName}-${var.Environment}-lambda-consumer-iot"
  assume_role_policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = {
          Service = ["lambda.amazonaws.com"]
        }
      },
    ]
  })
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSTransferLoggingAccess"]
  inline_policy {
    name   = "cloudwatch"
    policy = jsonencode({
      Version   = "2012-10-17"
      Statement = [
        {
          Action   = ["logs:CreateLogGroup", "logs:CreateLogStream", "logs:PutLogEvents"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
  inline_policy {
    name   = "xray"
    policy = jsonencode({
      Version   = "2012-10-17"
      Statement = [
        {
          Action = [
            "xray:PutTraceSegments",
            "xray:PutTelemetryRecords",
            "xray:GetSamplingRules",
            "xray:GetSamplingTargets",
            "xray:GetSamplingStatisticSummaries"
          ]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
  inline_policy {
    name   = "dynamodb"
    policy = jsonencode({
      Version   = "2012-10-17"
      Statement = [
        {
          Action = [
            "dynamodb:GetItem",
            "dynamodb:DeleteItem",
            "dynamodb:PutItem",
            "dynamodb:Scan",
            "dynamodb:Query"
          ]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
  tags = {
    ProjectCode = "${var.ProjectCode}",
    ProjectName = "${var.ProjectName}",
    Environment = "${var.Environment}"
  }
}

resource "aws_iam_role" "lambda_api_role" {
  name               = "${var.ProjectCode}-${var.ProjectName}-${var.Environment}-lambda-api-iot"
  assume_role_policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = {
          Service = ["lambda.amazonaws.com"]
        }
      },
    ]
  })
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSTransferLoggingAccess"]
  inline_policy {
    name   = "cloudwatch"
    policy = jsonencode({
      Version   = "2012-10-17"
      Statement = [
        {
          Action   = ["logs:CreateLogGroup", "logs:CreateLogStream", "logs:PutLogEvents"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
  inline_policy {
    name   = "xray"
    policy = jsonencode({
      Version   = "2012-10-17"
      Statement = [
        {
          Action = [
            "xray:PutTraceSegments",
            "xray:PutTelemetryRecords",
            "xray:GetSamplingRules",
            "xray:GetSamplingTargets",
            "xray:GetSamplingStatisticSummaries"
          ]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
  tags = {
    ProjectCode = "${var.ProjectCode}",
    ProjectName = "${var.ProjectName}",
    Environment = "${var.Environment}"
  }
}

############## Kinesis #################
resource "aws_kinesis_stream" "stream" {
  name = "${var.ProjectCode}-${var.ProjectName}-stream-iot"
  stream_mode_details {
    stream_mode = "ON_DEMAND"
  }
  tags = {
    ProjectCode = "${var.ProjectCode}",
    ProjectName = "${var.ProjectName}",
    Environment = "${var.Environment}"
  }
}

resource "aws_kinesis_stream_consumer" "consumer" {
  name       = "${var.ProjectCode}-${var.ProjectName}-stream-consumer-iot"
  stream_arn = aws_kinesis_stream.stream.arn
}

############## Lambdas ##############
data "archive_file" "lambda_zip_inline" {
  type        = "zip"
  output_path = "/tmp/lambda_zip_inline.zip"
  source {
    content  = "exports.handler = async (event) => ({body:'Pending on upload'})"
    filename = "index.js"
  }
}

resource "aws_lambda_function" "lambda_api" {
  function_name = "${var.ProjectCode}-${var.ProjectName}-lambda-api-iot"
  role          = aws_iam_role.lambda_api_role.arn
  handler       = "index.handler"

  filename         = "${data.archive_file.lambda_zip_inline.output_path}"
  source_code_hash = "${data.archive_file.lambda_zip_inline.output_base64sha256}"

  runtime = "nodejs18.x"

  environment {
    variables = {
      ENVIRONMENT  = "${var.Environment}"
      PROJECT_CODE = "${var.ProjectCode}"
      PROJECT_NAME = "${var.ProjectName}"
    }
  }
}

resource "aws_lambda_permission" "lambda_api_permission" {
  statement_id  = "${var.ProjectCode}-${var.ProjectName}-lambda-permission-api-iot"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_api.function_name}"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.api_rest.execution_arn}/*"
}

resource "aws_lambda_function" "lambda_consumer" {
  function_name = "${var.ProjectCode}-${var.ProjectName}-lambda-consumer-iot"
  role          = aws_iam_role.lambda_api_kinesis_role.arn
  handler       = "index.handler"

  filename         = "${data.archive_file.lambda_zip_inline.output_path}"
  source_code_hash = "${data.archive_file.lambda_zip_inline.output_base64sha256}"

  runtime = "nodejs18.x"

  environment {
    variables = {
      ENVIRONMENT  = "${var.Environment}"
      PROJECT_CODE = "${var.ProjectCode}"
      PROJECT_NAME = "${var.ProjectName}"
    }
  }
}

resource "aws_lambda_event_source_mapping" "lambda_consumer_event" {
  event_source_arn  = aws_kinesis_stream.stream.arn
  function_name     = aws_lambda_function.lambda_consumer.arn
  starting_position = "LATEST"
  batch_size        = 1
}

resource "aws_lambda_function" "lambda_notify" {
  function_name = "${var.ProjectCode}-${var.ProjectName}-lambda-consumer-iot"
  role          = aws_iam_role.lambda_api_kinesis_role.arn
  handler       = "index.handler"

  filename         = "${data.archive_file.lambda_zip_inline.output_path}"
  source_code_hash = "${data.archive_file.lambda_zip_inline.output_base64sha256}"

  runtime = "nodejs18.x"

  environment {
    variables = {
      ENVIRONMENT  = "${var.Environment}"
      PROJECT_CODE = "${var.ProjectCode}"
      PROJECT_NAME = "${var.ProjectName}"
    }
  }
}

resource "aws_lambda_event_source_mapping" "lambda_notify_event" {
  event_source_arn  = aws_sqs_queue.sqs_notify.arn
  function_name     = aws_lambda_function.lambda_notify.arn
  starting_position = "LATEST"
  batch_size        = 10
}

############## Sqs #################
resource "aws_sqs_queue" "sqs_notify" {
  name                      = "${var.ProjectCode}-${var.ProjectName}-Notify-${var.Environment}-sqs"
  delay_seconds             = 0
  message_retention_seconds = 86400
  receive_wait_time_seconds = 0
  redrive_policy            = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.sqs_notify_dead.arn
    maxReceiveCount     = 4
  })
  tags = {
    ProjectCode = "${var.ProjectCode}",
    ProjectName = "${var.ProjectName}",
    Environment = "${var.Environment}"
  }
}

resource "aws_sqs_queue" "sqs_notify_dead" {
  name                      = "${var.ProjectCode}-${var.ProjectName}-Notify-${var.Environment}-dlq"
  message_retention_seconds = 86400
  tags                      = {
    ProjectCode = "${var.ProjectCode}",
    ProjectName = "${var.ProjectName}",
    Environment = "${var.Environment}"
  }
}

############## Api #################
resource "aws_api_gateway_rest_api" "api_rest" {
  body = jsonencode({
    openapi = "3.0.1"
    info    = {
      title   = "Service"
      version = "1.0"
    }
    paths = {
      "/anypath/{param}/anypath" = {
        description = "any method"
        parameters  = [
          {
            in     = "path"
            name   = "param"
            name   = "param"
            schema = {
              type    = "string"
              example = "1213213213212"
            }
          }
        ]
        post = {
          x-amazon-apigateway-integration = {
            httpMethod           = "POST"
            payloadFormatVersion = "1.0"
            type                 = "aws_proxy"
            uri                  = "arn:aws:apigateway:${var.Region}:lambda:path/2015-03-31/functions/${aws_lambda_function.lambda_api.arn}/invocations"
          }
        }
      }
    }
  })

  name = "${var.ProjectCode}-${var.ProjectName}-RestApi"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_deployment" "api_rest_deploy" {
  rest_api_id = aws_api_gateway_rest_api.api_rest.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.api_rest.body))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "api_rest_state" {
  deployment_id = aws_api_gateway_deployment.api_rest_deploy.id
  rest_api_id   = aws_api_gateway_rest_api.api_rest.id
  stage_name    = var.Stage
}